from setuptools import setup, find_packages

setup(
    name='logging_config',
    version='0.1.0',
    packages=find_packages(),
    description='Reusable logging configuration package',
    author='Jan Bukała',
    author_email='jan.bukala@prodel.com.pl',
    url='https://gitlab.com/janbukalaprodel/logging-config'
)