# logging_config/config.py
import os
import logging
from logging.handlers import RotatingFileHandler
def configure_logging(
        app_name='app',
        log_level_console=logging.INFO,
        log_level_file=logging.DEBUG,
        log_file='logs/app.log',
        log_format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        max_bytes=10 * 1024 * 1024,
        backup_count=5,
):
    # Create the main logger
    logger = logging.getLogger(app_name)
    logger.setLevel(logging.DEBUG)

    # Ensure the logs directory exists
    log_dir = os.path.dirname(log_file)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # Console Handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(log_level_console)
    console_handler.setFormatter(logging.Formatter(log_format))

    # Rotating File Handler
    file_handler = RotatingFileHandler(log_file, maxBytes=max_bytes, backupCount=backup_count)
    file_handler.setLevel(log_level_file)
    file_handler.setFormatter(logging.Formatter(log_format))

    # Add handlers to logger
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)

    return logger